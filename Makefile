CONTINOUS="-pvc"
NOTCONT="-pv"
TEXARG="--shell-escape --halt-on-error --interaction=batchmode --output-directory=build"
TEX="env $(TEXENV) latexmk -lualatex $(TEXARG)"

all: build/WinkelmannEtAl2017.pdf

diff: build/WinkelmannEtAl2017Diff.pdf

correction: WinkelmannEtAl2017Corrections.tex
	mkdir -p build
	eval $(TEX) $(NOTCONT) --jobname="WinkelmannEtAl2017Corrections" "WinkelmannEtAl2017Corrections" > /dev/null 2>&1 &

build/WinkelmannEtAl2017.pdf: WinkelmannEtAl2017.tex figures
	mkdir -p build
	eval $(TEX) $(CONTINOUS) --jobname="WinkelmannEtAl2017" "WinkelmannEtAl2017" > /dev/null 2>&1 &

WinkelmannEtAl2017Diff.tex: WinkelmannEtAl2017.tex WinkelmannEtAl2017Corrections.tex
	latexdiff WinkelmannEtAl2017.tex WinkelmannEtAl2017Corrections.tex > WinkelmannEtAl2017Diff.tex

build/WinkelmannEtAl2017Diff.pdf: WinkelmannEtAl2017Diff.tex
	mkdir -p build
	eval $(TEX) $(NOTCONT) --jobname="WinkelmannEtAl2017Diff" "WinkelmannEtAl2017Diff" > /dev/null 2>&1 &

clean:
	 rm -rf build
	 rm WinkelmannEtAl2017Diff.tex

.PHONY: clean diff
